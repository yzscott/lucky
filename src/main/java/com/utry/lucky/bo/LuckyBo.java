package com.utry.lucky.bo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * @Auther: yangzhen
 * @Date: 2019/4/9 22:43
 * @Description:
 */
@Entity
public class LuckyBo {
    @Id
    @GeneratedValue
    private Integer id;
    private BigDecimal money;
    private String producer;
    private String consumer;

    public LuckyBo() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getConsumer() {
        return consumer;
    }

    public void setConsumer(String consumer) {
        this.consumer = consumer;
    }
}
