package com.utry.lucky.controller;

import com.utry.lucky.bo.LuckyBo;
import com.utry.lucky.dao.LuckyDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * @Auther: yangzhen
 * @Date: 2019/4/9 23:08
 * @Description:
 */
@RestController
public class LuckyController {

    @Autowired
    private LuckyDao luckyDao;


    @RequestMapping("/list")
    public List<LuckyBo> list(){
        return luckyDao.findAll();
    }

    @PostMapping("/create")
    public LuckyBo create(@RequestParam(value = "producer") String producer, BigDecimal money){
        LuckyBo luckyBo = new LuckyBo();
        luckyBo.setMoney(money);
        luckyBo.setProducer(producer);
        return luckyDao.save(luckyBo);
    }

    @GetMapping("/findById/{id}")
    public LuckyBo findById(@PathVariable(value = "id") Integer id){
        return luckyDao.findById(id).orElse(null);
    }

    @PutMapping("/updateById/{id}")
    public LuckyBo updateById(@PathVariable(value = "id") Integer id,String consumer){
        Optional<LuckyBo> optional = luckyDao.findById(id);
        if(optional.isPresent()){
            LuckyBo luckyBo = optional.get();
            luckyBo.setConsumer(consumer);
            luckyBo.setId(id);
            return luckyDao.save(luckyBo);
        }
        return null;
    }
}
