package com.utry.lucky.dao;


import com.utry.lucky.bo.LuckyBo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Auther: yangzhen
 * @Date: 2019/4/9 23:09
 * @Description:
 */

public interface LuckyDao extends JpaRepository<LuckyBo,Integer> {

}
